<?php

namespace Drupal\Tests\feedback\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\feedback\Entity\FeedbackMessage;

/**
 * Test the feedback module's basic functionality.
 *
 * @group feedback
 */
class BasicFunctionalityTest extends BrowserTestBase {

  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['feedback', 'block'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The help text for the feedback form.
   *
   * @var string
   */
  protected string $helpText;

  /**
   * The submit button text for the feedback form.
   *
   * @var string
   */
  protected string $submitText;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->helpText = \trim($this->getRandomGenerator()->paragraphs(1));
    $this->submitText = $this->randomString();
    $this->placeBlock('feedback_block', [
      'id' => 'test_feedback_block',
      'feedback_type' => 'default_feedback',
      'feedback_help' => $this->helpText,
      'feedback_submit' => $this->submitText,
    ]);
  }

  /**
   * Test that the user cannot send feedback without permission.
   */
  public function testCannotSendFeedbackWithoutPermission(): void {
    // Setup: Create a user without permission to send feedback.
    $this->drupalLogin($this->createUser());

    // System under test: Load a page with a feedback form.
    $this->drupalGet('<front>');

    // Assertions: Check the key components of the feedback form are not present
    // on the page.
    $this->assertSession()->elementNotExists('xpath', $this->assertSession()->buildXPathQuery('//div[@id="block-test-feedback-block"]'), NULL);
    $this->assertSession()->pageTextNotContains($this->helpText);
  }

  /**
   * Test that a user can send feedback through the form.
   */
  public function testSendFeedbackWithPermission(): void {
    // Setup: Create a user with permission to send feedback.
    $this->drupalLogin($this->createUser(['add feedback message entities']));

    // System under test: Load a page with a feedback form.
    $this->drupalGet('<front>');

    // Assertions: Check the key components of the feedback form are present on
    // the page.
    $block = $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//div[@id="block-test-feedback-block"]'), NULL);
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//details'), $block);
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//details/summary'), $block);
    $form = $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//details//form[@id=:form_id]', [
      ':form_id' => 'feedback-message-default-feedback-form',
    ]), $block);
    $this->assertSession()->pageTextContains($this->helpText);
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//textarea[@name="body[0][value]"]'), $form);
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//input[@type="submit" and @value=:submitText]', [
      ':submitText' => $this->submitText,
    ]));

    // System under test: Generate a feedback message, then submit the form.
    $feedbackMessage = \trim($this->getRandomGenerator()->paragraphs(1));
    $this->submitForm([
      'body[0][value]' => $feedbackMessage,
    ], $this->submitText);

    // Assertions: Check that we got the status message we expected.
    $this->assertSession()->statusMessageContains('Thank you for your feedback.');

    // Assertions: Check that we can load the feedback message that was just
    // submitted.
    $messages = $this->container->get('entity_type.manager')->getStorage('feedback_message')->loadByProperties([
      'body' => $feedbackMessage,
    ]);
    $this->assertIsArray($messages);
    $this->assertCount(1, $messages);
    $submittedMessage = reset($messages);
    $this->assertInstanceOf(FeedbackMessage::class, $submittedMessage);
    $this->assertEquals($feedbackMessage, $submittedMessage->get('body')->first()->getValue()['value']);
  }

}
