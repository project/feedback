<?php

namespace Drupal\Tests\feedback\Functional;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\feedback\Entity\FeedbackMessageType;

/**
 * Test that feedback message types can be added, viewed, changed, and deleted.
 *
 * @group feedback
 */
class CreateReadUpdateDeleteMessageTypeTest extends BrowserTestBase {

  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['feedback', 'block', 'field_ui'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A feedback message type that we will use during the tests.
   *
   * @var \Drupal\feedback\Entity\FeedbackMessageType
   */
  protected FeedbackMessageType $testType;

  /**
   * The ID of the feedback message type that we will use during the tests.
   *
   * @var string
   */
  protected string $testTypeId;

  /**
   * The success message of the feedback message type we will use during tests.
   *
   * @var \Drupal\feedback\Entity\FeedbackMessageType
   */
  protected string $testTypeSuccessMessage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Setup: Programmatically create a feedback message type for this test.
    $this->testTypeId = $this->randomMachineName();
    $this->testTypeSuccessMessage = \trim($this->getRandomGenerator()->sentences(5));
    $this->testType = FeedbackMessageType::create([
      'id' => $this->testTypeId,
      'label' => $this->testTypeId,
      'success_message' => $this->testTypeSuccessMessage,
    ]);
    $this->testType->save();

    // System under test: Try placing a block of that type.
    $this->placeBlock('feedback_block', [
      'id' => 'test_feedback_block',
      'feedback_type' => $this->testTypeId,
      'feedback_help' => 'Help text',
      'feedback_submit' => 'Submit feedback',
    ]);

    // Setup: Log in as a user with permission to administer feedback entities.
    $this->drupalLogin($this->createUser([
      'administer site configuration',
    ]));

  }

  /**
   * Test a user with correct permissions can create a feedback message type.
   */
  public function testCreateMessageType(): void {
    // Setup: Navigate to a page with a feedback message type form.
    $this->drupalGet(Url::fromRoute('entity.feedback_message_type.add_form'));

    // System under test: Enter information for a new feedback message type.
    $newTypeId = $this->randomMachineName();
    $newTypeLabel = $this->randomString();
    $newTypeSuccessMessage = \trim($this->getRandomGenerator()->sentences(5));
    $this->submitForm([
      'id' => $newTypeId,
      'label' => $newTypeLabel,
      'success_message' => $newTypeSuccessMessage,
    ], 'Save');

    // Assertions: Check that we cna load the feedback message type that was
    // just submitted.
    $newMessageType = $this->getMessageTypeStorage()->load($newTypeId);
    $this->assertInstanceOf(FeedbackMessageType::class, $newMessageType);
    $this->assertEquals($newTypeLabel, $newMessageType->get('label'));
    $this->assertEquals($newTypeSuccessMessage, $newMessageType->get('success_message'));
  }

  /**
   * Test a user with correct permissions can delete a feedback message type.
   */
  public function testDeleteMessageType(): void {
    // Setup: Navigate to the delete form for the message type.
    $this->drupalGet($this->testType->toUrl('delete-form'));

    // System under test: Submit the form.
    $this->submitForm([], 'Delete');

    // Assertions: Check that the message type was deleted.
    $this->assertNull($this->getMessageTypeStorage()->load($this->testTypeId));
  }

  /**
   * Test that the Feedback Message Type list works as-intended.
   */
  public function testListMessageTypes(): void {
    // System under test: Load the Feedback Message Type list page.
    $this->drupalGet(Url::fromRoute('entity.feedback_message_type.collection'));

    // Assertions: Check that the feedback message type list appears.
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//table//th[1][text()="Feedback message type"]'));
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//table//th[2][text()="Machine name"]'));
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery('//table//th[3][text()="Operations"]'));

    // Assertions: Check there is a row for the message type we created in the
    // setUp.
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery("//table//tr/td[1][.=:type_id]", [
      ':type_id' => $this->testTypeId,
    ]));
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery("//table//tr/td[2][.=:type_id]", [
      ':type_id' => $this->testTypeId,
    ]));
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery("//table//tr/td[3]//a[starts-with(@href,:edit_url)]", [
      ':edit_url' => $this->testType->toUrl('edit-form')->toString(),
    ]));
    $this->assertSession()->elementExists('xpath', $this->assertSession()->buildXPathQuery("//table//tr/td[3]//a[starts-with(@href,:delete_url)]", [
      ':delete_url' => $this->testType->toUrl('delete-form')->toString(),
    ]));
  }

  /**
   * Test that a user with correct permissions can edit a feedback message type.
   */
  public function testUpdateMessageType(): void {
    // System under test: Navigate to the edit form for the message type.
    $this->drupalGet($this->testType->toUrl('edit-form'));

    // Assertions: Check that we can see the fields we expect.
    $this->assertSession()->fieldValueEquals('label', $this->testTypeId);
    $this->assertSession()->fieldValueEquals('success_message', $this->testTypeSuccessMessage);

    // System under test: Edit some fields and submit the form.
    $newTypeName = $this->randomString();
    $newSuccessMessage = \trim($this->getRandomGenerator()->sentences(5));
    $this->submitForm([
      'label' => $newTypeName,
      'success_message' => $newSuccessMessage,
    ], 'Save');

    // Assertions: Check that the message type was changed.
    $this->assertSession()->statusMessageContains("Saved the $newTypeName Feedback message type.");
    $this->drupalGet(Url::fromRoute('entity.feedback_message_type.edit_form', [
      'feedback_message_type' => $this->testTypeId,
    ]));
    $this->assertSession()->fieldValueEquals('label', $newTypeName);
    $this->assertSession()->fieldValueEquals('success_message', $newSuccessMessage);
  }

  /**
   * Get an entity storage interface for feedback message types.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   An entity storage interface for feedback message types.
   */
  protected function getMessageTypeStorage(): EntityStorageInterface {
    return $this->container->get('entity_type.manager')->getStorage('feedback_message_type');
  }

}
