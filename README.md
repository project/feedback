# Feedback

Feedback allows visitors and users of a site to report issues for the currently
displayed page, where issues could be data or theming related bugs, or also
feature requests.

Site administrators are then able to review and process those feedback messages.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Configure the user permissions in Administration » People » Permissions
2. Configure fields at Administration » Configuration » User interface »
  Feedback.
3. To view all feedback messages, go to Administration » Reports » Feedback
   messages.
