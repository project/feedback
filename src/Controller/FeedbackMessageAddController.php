<?php

namespace Drupal\feedback\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Dedicated controller to add content entities with bundles.
 *
 * @package Drupal\feedback\Controller
 */
class FeedbackMessageAddController extends ControllerBase {

  /**
   * An entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Displays add links for available bundles/types for entity feedback_message.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A render array for a list of the feedback_message bundles/types that can
   *   be added or if there is only one type/bundle defined for the site, the
   *   function returns the add page for that bundle/type.
   */
  public function add(Request $request) {
    $types = $this->getMessageTypeStorage()->loadMultiple();
    if ($types && count($types) == 1) {
      $type = reset($types);
      return $this->addForm($type, $request);
    }
    if (count($types) === 0) {
      return [
        '#markup' => $this->t('You have not created any %bundle types yet. @link to add a new type.', [
          '%bundle' => 'Feedback message',
          '@link' => Link::fromTextAndUrl($this->t('Go to the type creation page'), Url::fromRoute('entity.feedback_message_type.add_form')),
        ]),
      ];
    }
    return [
      '#theme' => 'feedback_message_content_add_list',
      '#content' => $types,
    ];
  }

  /**
   * Presents creation form for feedback_message entities of given bundle/type.
   *
   * @param \Drupal\Core\Entity\EntityInterface $feedback_message_type
   *   The custom bundle to add.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A form array as expected by drupal_render().
   */
  public function addForm(EntityInterface $feedback_message_type, Request $request) {
    $entity = $this->getMessageStorage()->create([
      'type' => $feedback_message_type->id(),
    ]);
    return $this->entityFormBuilder()->getForm($entity);
  }

  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\Core\Entity\EntityInterface $feedback_message_type
   *   The custom bundle/type being added.
   *
   * @return string
   *   The page title.
   */
  public function getAddFormTitle(EntityInterface $feedback_message_type) {
    return $this->t('Create of bundle @label', [
      '@label' => $feedback_message_type->label(),
    ]);
  }

  /**
   * Get an entity storage manager for feedback messages.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   Entity storage for feedback messages.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   */
  protected function getMessageStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('feedback_message');
  }

  /**
   * Get an entity storage manager for feedback message types.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   Entity storage for feedback message types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   */
  protected function getMessageTypeStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('feedback_message_type');
  }

}
