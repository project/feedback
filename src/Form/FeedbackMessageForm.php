<?php

namespace Drupal\feedback\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Feedback message edit forms.
 *
 * @ingroup feedback
 */
class FeedbackMessageForm extends ContentEntityForm {

  /**
   * An entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\feedback\Entity\FeedbackMessage $entity */
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        /** @var FeedbackMessageType $message_type */
        $message_type = $this->getFeedbackMessageTypeStorage()->load($entity->getType());
        $this->messenger()->addStatus($message_type->getSuccessMessage());
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Feedback message.', [
          '%label' => $entity->label(),
        ]));
    }

    return $status;
  }

  /**
   * Get entity storage feedback message types.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The feedback_message_type storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   */
  protected function getFeedbackMessageTypeStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('feedback_message_type');
  }

}
